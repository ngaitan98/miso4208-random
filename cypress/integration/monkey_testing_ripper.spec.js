describe('Los estudiantes under monkeys', function () {
    it('visits los estudiantes and survives monkeys', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomEvent(10);
    })
})
function randomClick(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };
    console.log(monkeysLeft)
    var ml = monkeysLeft;
    if (monkeysLeft > 0) {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            if (!Cypress.dom.isHidden(randomLink)) {
                cy.wrap(randomLink).click({ force: true });
                ml = ml - 1;
            }
            setTimeout(randomClick(ml), 1000);
        });
    }
}
function randomEvent(monkeysLeft) {
    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };
    function getRandomString(){
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }
    console.log(monkeysLeft)
    var ml = monkeysLeft;
    if (monkeysLeft > 0) {
        var instruction = getRandomInt(1, 4)
        console.log("Instruction: " + instruction);
        if (instruction === 1) {
            cy.get('a').then($links => {
                var randomLink = $links.get(getRandomInt(0, $links.length));
                if (!Cypress.dom.isHidden(randomLink)) {
                    cy.wrap(randomLink).click({ force: true });
                    ml = ml - 1;
                }
                setTimeout(randomEvent(ml), 1000);
            });
        }
        else if (instruction === 2) {
            cy.get('button').then($btns => {
                var randomBtn = $btns.get(getRandomInt(0, $btns.length));
                if (!Cypress.dom.isHidden(randomBtn)) {
                    cy.wrap(randomBtn).click({ force: true });
                    ml = ml - 1;
                }
                setTimeout(randomEvent(ml), 1000);
            });
        }
        else if (instruction === 3) {
            cy.get('input').then($inputs => {
                var randomInput = $inputs.get(getRandomInt(0, $inputs.length));
                cy.wrap(randomInput).click({force:true}).type(getRandomString(), { force: true });
                ml = ml - 1;
                setTimeout(randomEvent(ml), 1000);
            });
        } 
        else {
            cy.get('input[type="checkbox"]').then($inputs => {
                var randomInput = $inputs.get(getRandomInt(0, $inputs.length));
                cy.wrap(randomInput).click({force:true});
                ml = ml - 1;
                setTimeout(randomEvent(ml), 1000);
            });        
        }
    }
}